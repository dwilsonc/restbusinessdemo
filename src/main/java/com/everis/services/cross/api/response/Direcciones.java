package com.everis.services.cross.api.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Direcciones {
   
  private Personal personal;
  private Laboral laboral;
  private fiscal fiscal;

}
