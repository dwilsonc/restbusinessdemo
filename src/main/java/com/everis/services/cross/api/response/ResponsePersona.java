package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModelProperty;

public class ResponsePersona {
	
	@ApiModelProperty(name = "corporateLoanId", value = "Código del crédito Consist.", dataType = "String", example = "D47500049076")
	  @Valid
	  private String corporateLoanId;
}
