package com.everis.services.cross.api.response;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(value = Include.NON_NULL)
@ApiModel(description = "Contiene los productos.")
public class Product implements Serializable {

  private static final long serialVersionUID = -7886270810040546358L;

  private TioAux tioaux;

}