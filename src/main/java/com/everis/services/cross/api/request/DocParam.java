package com.everis.services.cross.api.request;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ApiModel(description = "Clase contenedora de los parametros que el API acepta.")
@ToString
public class DocParam {
	
	@Size(min = 8, max = 11, message = "El campo debe tener mínimo 8 y máximo 11 caracteres.")
	  @ApiParam(required = true, value = "doc.", example = "10468466916" + "",
	      hidden = true, name = "doc")
	  @Valid
	
	private String doc;
}
