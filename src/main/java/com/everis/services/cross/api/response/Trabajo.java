package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
    description = "Contiene los datos de puesto de trabajo")
public class Trabajo {
  @ApiModelProperty(name="Lugar",value="Lugar donde trabaja", dataType="String",example="Trujillo" )
  @Valid
  private String lugar;
  @ApiModelProperty(name="descripcion",value="Descripcion del puesto", dataType="String",example="Junior")
  @Valid
  private String descripcion;
}
