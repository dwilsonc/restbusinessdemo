package com.everis.services.cross.api.response;

import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ApiModel(
description = "Contiene los datos del estudiante")
public class Persona {

	@ApiModelProperty(name = "nombre", value = "Nombre del estudiante", dataType = "String", example = "Dennis")
	@Valid
	private String nombre;
	
	@ApiModelProperty(name = "apellidos", value = "Apellidos del estudiante", dataType = "String", example = "Wilson")
	@Valid
	private String apellidos;
}
