package com.everis.services.cross.api.response;

import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
public class Laboral {
  @ApiModelProperty(name = "descripcion", value = " descripcion direccion ", dataType = "String", example = "Trujillo Av. Condorcanqui")
  @Valid
  private String descripcion;
}
