package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ApiModel(
    description = "Contiene las direcciones de las empresas")
public class Direccion {
	
	@ApiModelProperty(name = "fiscal", value = "Direccion fiscal", dataType = "String", example = "Av américa sur 876 Sto Dom")
	@Valid
	private String direccion_fiscal;
	
	@ApiModelProperty(name = "personal", value = "Direccion personal", dataType = "String", example = "Seneca 459 La Noria")
	@Valid
	private String direccion_personal;
	
	@ApiModelProperty(name = "laboral", value = "Direccion laboral", dataType = "String", example = "Orbegoso 352 Trujillo centro")
	@Valid
	private String direccion_laboral;

}
