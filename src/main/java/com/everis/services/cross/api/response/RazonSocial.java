package com.everis.services.cross.api.response;


import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ApiModel(
    description = "Contiene los razon social")
public class RazonSocial {
  @ApiModelProperty(name = "correo", value = "Correo del estudiante", dataType = "String", example = "dwilsonc@everis.com")
  @Valid
  private String correo;
  private Documento documento;
  private Persona persona;
  
 
 
  
   private Direcciones direcciones;
}
