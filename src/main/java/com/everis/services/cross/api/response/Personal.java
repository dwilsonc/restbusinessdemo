package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Personal {
  @ApiModelProperty(name = "descripcion", value = " descripcion direccion ", dataType = "String", example = "Trujillo Av. peru")
  @Valid
  private String descripcion;
}
