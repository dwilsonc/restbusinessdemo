package com.everis.services.cross.api.response;

import java.util.List;

import javax.validation.Valid;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
    description = "Contiene los datos del estudiante")
public class ResponseEstudiante {

	@ApiModelProperty(name = "correo", value = "Correo del estudiante", dataType = "String", example = "dwilsonc@everis.com")
	@Valid
	private String correo;
	
	@ApiModelProperty(name = "documento", value = "TIpo documento del estudiante", dataType = "String", example = "dni")
	@Valid
	private String documento;
	
	@ApiModelProperty(name = "numerodocumento", value = "Numero de documento del estudiante", dataType = "String", example = "46090495")
	@Valid
	private String numerodocumento;
	
	private Persona persona;
	
	private List<Curso> cursos;
	
	private Trabajo trabajo;
	 
}
