package com.everis.services.cross.api.response;

import java.io.Serializable;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
    description = "Contiene DebtType asociado.")
@JsonInclude(
    value = Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class DebtType implements Serializable {

  private static final long serialVersionUID = -4720509794646945903L;

  @ApiModelProperty(
      name = "code",
      value = "Indicador de tipo de deuda.",
      dataType = "string",
      example = "DIR")
  @Valid
  private String code;

}
