package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
    description = "Contiene los datos de los cursos del estudiante")
public class Curso {

	@ApiModelProperty(name = "codigo", value = "Codigo del curso del estudiante", dataType = "String", example = "java8")
	@Valid
	private String codigo;
	
	@ApiModelProperty(name = "descripcion", value = "Descripcion del curso del estudiante", dataType = "String", example = "Curso inicial de java 8")
	@Valid
	private String descripcion;
	
	@ApiModelProperty(name = "horas", value = "Horas de duración del curso", dataType = "String", example = "72 horas")
	@Valid
	private String horas;
}