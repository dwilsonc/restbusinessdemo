package com.everis.services.cross.api.response;

import javax.validation.Valid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
description = "Contiene info de los documentos")
public class Documento {

		@ApiModelProperty(name = "tipo", value = "Tipo de documento", dataType = "String", example = "RUC")
		@Valid
		private String tipo;
		
		@ApiModelProperty(name = "numerodocumento", value = "Número del documento", dataType = "String", example = "46846691")
		@Valid
		private String numerodocumento;
}
