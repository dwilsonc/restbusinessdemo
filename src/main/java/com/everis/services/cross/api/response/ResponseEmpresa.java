package com.everis.services.cross.api.response;

import java.util.List;
import javax.validation.Valid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(
description = "Contiene los datos de la empresa")
public class ResponseEmpresa {
	
	@ApiModelProperty(name = "correo", value = "Correo de la empresa", dataType = "String", example = "contacto@everis.com")
	@Valid
	private String correo;
	
	private List<Documento> documento;
	
	private Persona persona;
	
	private List<Direccion> direccion;

}
