package com.everis.services.cross.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.services.cross.api.response.Curso;
import com.everis.services.cross.api.response.DebtType;
import com.everis.services.cross.api.response.Direcciones;
import com.everis.services.cross.api.response.Documento;
import com.everis.services.cross.api.response.Laboral;
import com.everis.services.cross.api.response.Persona;
import com.everis.services.cross.api.response.Personal;
import com.everis.services.cross.api.response.Product;
import com.everis.services.cross.api.response.RazonSocial;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.api.response.ResponseEstudiante;
import com.everis.services.cross.api.response.TioAux;
import com.everis.services.cross.api.response.Trabajo;
import com.everis.services.cross.api.response.fiscal;
import com.everis.services.cross.config.ApplicationProperties;
import com.everis.services.cross.service.ApiCrossService;

import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ApiCrossServiceImpl implements ApiCrossService {

	@Autowired
	private ApplicationProperties properties;
	
	@Override
	public Single<Response> obtenerRespuestaCorporateLoans(String corporateLoanId) {
		Map<String, Single<Response>> responseMapIni = constructResponseObject(corporateLoanId);
		return responseMapIni.entrySet().stream().filter(e -> e.getKey().equals(corporateLoanId))
				.map(Map.Entry::getValue).findFirst().orElse(null);
	}

	private Map<String, Single<Response>> constructResponseObject(String corporateLoanId) {
		Map<String, Single<Response>> responseMap = new HashMap<String, Single<Response>>();
		responseMap.put("D47500049014", getResponse1());
		responseMap.put("D47500049012", getResponse2());
		responseMap.put("D47500049013", getResponse3());
		return responseMap;
	}

	private Single<Response> getResponse1() {
		Response response = new Response();
		response.setCorporateLoanId("D47500049014");
		response.setDebtType(new DebtType());
		response.getDebtType().setCode("CTG14");
		response.setProductDetail(new Product());
		response.getProductDetail().setTioaux(new TioAux());
		response.getProductDetail().getTioaux().setCode("FWDCOM14");
		return Single.fromCallable(() -> response);
	}

	private Single<Response> getResponse2() {
		Response response = new Response();
		response.setCorporateLoanId("D47500049012");
		response.setDebtType(new DebtType());
		response.getDebtType().setCode("CTG12");
		response.setProductDetail(new Product());
		response.getProductDetail().setTioaux(new TioAux());
		response.getProductDetail().getTioaux().setCode("FWDCOM12");
		return Single.fromCallable(() -> response);
	}

	private Single<Response> getResponse3() {
		Response response = new Response();
		response.setCorporateLoanId("D47500049013");
		response.setDebtType(new DebtType());
		response.getDebtType().setCode("CTG13");
		response.setProductDetail(new Product());
		response.getProductDetail().setTioaux(new TioAux());
		response.getProductDetail().getTioaux().setCode("FWDCOM13");
		return Single.fromCallable(() -> response);
	}
//Mod
	@Override 
	public Single<ResponseEstudiante> obtenerRespuestaTrabajo(String dni){
	  Map<String, Single<ResponseEstudiante>> respnseMapIni=constructResponseTrabajoObject(dni);
	  return respnseMapIni.entrySet().stream()
	      .filter(e->e.getKey().equals(dni))
	      .map(Map.Entry::getValue)
	      .findFirst()
	      .orElse(null);
	}
	private Map<String, Single<ResponseEstudiante>> constructResponseTrabajoObject(String dni){
	  Map<String, Single<ResponseEstudiante>> responseMapZ = new HashMap<String,Single<ResponseEstudiante>>();
	  responseMapZ.put(properties.getDni1(), getResponseTrabajo1());
	  responseMapZ.put(properties.getDni2(), getResponseTrabajo2());
	  responseMapZ.put(properties.getDni3(), getResponseTrabajo3());
	  return responseMapZ;
	  
	}
	private Single<ResponseEstudiante> getResponseTrabajo1(){
	  ResponseEstudiante response = new ResponseEstudiante();
	  Trabajo trabajo = new Trabajo();
	  trabajo.setDescripcion("Jefe de area");
	  trabajo.setLugar("Lima");
	  response.setTrabajo(trabajo);
      response.setDocumento("dni");
      response.setNumerodocumento("44112251");
	  return Single.fromCallable(() -> response);
	}
	private Single<ResponseEstudiante> getResponseTrabajo2(){
      ResponseEstudiante response = new ResponseEstudiante();
      Trabajo trabajo = new Trabajo();
      trabajo.setDescripcion("Junior 1");
      trabajo.setLugar("Trujillo");
      response.setTrabajo(trabajo);
      response.setDocumento("dni");
      response.setNumerodocumento("44112252");
      return Single.fromCallable(() -> response);
    }
	private Single<ResponseEstudiante> getResponseTrabajo3(){
      ResponseEstudiante response = new ResponseEstudiante();
      Trabajo trabajo = new Trabajo();
      trabajo.setDescripcion("Developer");
      trabajo.setLugar("Chiclayo");
      response.setTrabajo(trabajo);
      response.setDocumento("dni");
      response.setNumerodocumento("44112253");
      return Single.fromCallable(() -> response);
    }
//
	@Override
    public Single<RazonSocial> obtenerRespuestaRazonSocial(String codigo) {
        Map<String, Single<RazonSocial>> responseMapIni = constructResponseRazonObject(codigo);
        return responseMapIni.entrySet().stream()
                .filter(e -> e.getKey().equals(codigo))
                .map(Map.Entry::getValue)
                .findFirst()
                .orElse(null);
    }
	private Map<String, Single<RazonSocial>> constructResponseRazonObject(String codigo) {
      Map<String, Single<RazonSocial>> responseMap = new HashMap<String, Single<RazonSocial>>();
      if (properties.getDni1().length()==8) {
        responseMap.put(properties.getDni1(), getResponseRazon());
      }
      if (properties.getDni2().length()==11) {
        responseMap.put(properties.getDni2(), getResponseRazon_emp());
      }
      //responseMap.put(properties.getDni1(), getResponseRazon());
      return responseMap;
  }
private Single<RazonSocial> getResponseRazon_emp() {
      
      RazonSocial response = new RazonSocial();
      response.setCorreo("dwilsonc@everis.com");
      Documento documento=new Documento();
      documento.setNumerodocumento("111111111111");
      documento.setTipo("ruc"); 
      response.setDocumento(documento);
      
      Persona persona =new Persona();
      persona.setNombre("Empresa SAC");
      
     
      response.setPersona(persona);
      

     
      fiscal dir_fiscal=new fiscal();
      dir_fiscal.setDescripcion("Av. Per� 333");
      
      Direcciones direccion1 = new Direcciones();
      
      direccion1.setFiscal(dir_fiscal);
     
      response.setDirecciones(direccion1);
      return Single.fromCallable(() -> response);
  }

	private Single<RazonSocial> getResponseRazon() {
	  
	  RazonSocial response = new RazonSocial();
	  response.setCorreo("dwilsonc@everis.com");
	  Documento documento=new Documento();
      documento.setNumerodocumento("44112251");
      documento.setTipo("dni"); 
      response.setDocumento(documento);
      
	  Persona persona =new Persona();
	  persona.setNombre("Dennis");
	  persona.setApellidos("Wilson");
	 
	  response.setPersona(persona);
	  

      Personal dir_personal=new Personal();
      dir_personal.setDescripcion("Av. Espa�a 1010");
      
      Laboral dir_laboral=new Laboral();
      dir_laboral.setDescripcion("Orbegoso 338");
      Direcciones direccion1 = new Direcciones();
      direccion1.setPersonal(dir_personal);
     
      direccion1.setLaboral(dir_laboral);   
      response.setDirecciones(direccion1);
      return Single.fromCallable(() -> response);
  }
	//
	@Override
	public Single<ResponseEstudiante> obtenerRespuestaDatosEstudiante(String dni) {
		Map<String, Single<ResponseEstudiante>> responseMapIni = constructResponseEstudianteObject(dni);
		return responseMapIni.entrySet().stream()
				.filter(e -> e.getKey().equals(dni))
				.map(Map.Entry::getValue)
				.findFirst()
				.orElse(null);
	}
	
	private Map<String, Single<ResponseEstudiante>> constructResponseEstudianteObject(String dni) {
		Map<String, Single<ResponseEstudiante>> responseMap = new HashMap<String, Single<ResponseEstudiante>>();
		log.info("DNI1: "+properties.getDni1());
		log.info("DNI2: "+properties.getDni2());
		log.info("DNI3: "+properties.getDni3());
		responseMap.put(properties.getDni1(), getResponseEstudiante1());
		responseMap.put(properties.getDni2(), getResponseEstudiante2());
		responseMap.put(properties.getDni3(), getResponseEstudiante3());
		return responseMap;
	}
	
	private Single<ResponseEstudiante> getResponseEstudiante1() {
		ResponseEstudiante response = new ResponseEstudiante();
		response.setPersona(new Persona());
		response.getPersona().setApellidos("Wilson");
		response.getPersona().setNombre("Dennis");
		response.setCorreo("dwilsonc@everis.com");
		Trabajo trabajo =new Trabajo();
		trabajo.setLugar("Arequipa");
		trabajo.setDescripcion("Lider");
		response.setTrabajo(trabajo);
		response.setDocumento("dni");
		response.setNumerodocumento("44112251");
		List<Curso> cursos = new ArrayList<Curso>();
		Curso curso1 = new Curso();
		curso1.setCodigo("java8");
		curso1.setDescripcion("Curso inicial de java 8");
		Curso curso2 = new Curso();
		curso2.setCodigo("angular");
		curso2.setDescripcion("Curso inicial de angular");
		Curso curso3 = new Curso();
		curso3.setCodigo("microservicios");
		curso3.setDescripcion("Curso con springboot y springcloud");
		cursos.add(curso1);
		cursos.add(curso2);
		cursos.add(curso3);
		response.setCursos(cursos);
		return Single.fromCallable(() -> response);
	}

	private Single<ResponseEstudiante> getResponseEstudiante2() {
		ResponseEstudiante response = new ResponseEstudiante();
		response.setPersona(new Persona());
		response.getPersona().setApellidos("Jean");
		response.getPersona().setNombre("Vasquez");
		response.setCorreo("jvasquez@everis.com");
		Trabajo trabajo=new Trabajo();
		trabajo.setLugar("Lima");
		trabajo.setDescripcion("Junio2");
		response.setTrabajo(trabajo);
		response.setDocumento("dni");
		response.setNumerodocumento("44112252");
		List<Curso> cursos = new ArrayList<Curso>();
		Curso curso1 = new Curso();
		curso1.setCodigo("java8");
		curso1.setDescripcion("Curso inicial de java 8");
		Curso curso2 = new Curso();
		curso2.setCodigo("angular");
		curso2.setDescripcion("Curso inicial de angular");
		Curso curso3 = new Curso();
		curso3.setCodigo("microservicios");
		curso3.setDescripcion("Curso con springboot y springcloud");
		cursos.add(curso1);
		cursos.add(curso2);
		cursos.add(curso3);
		response.setCursos(cursos);
		return Single.fromCallable(() -> response);
	}

	private Single<ResponseEstudiante> getResponseEstudiante3() {
		ResponseEstudiante response = new ResponseEstudiante();
		response.setPersona(new Persona());
		response.getPersona().setApellidos("Milton");
		response.getPersona().setNombre("Vasquez");
		response.setCorreo("mvasquez@everis.com");
		Trabajo trabajo =new Trabajo();
		trabajo.setLugar("Chiclayo");
		trabajo.setDescripcion("Developer");
		response.setTrabajo(trabajo);
		response.setDocumento("dni");
		response.setNumerodocumento("44112253");
		List<Curso> cursos = new ArrayList<Curso>();
		Curso curso1 = new Curso();
		curso1.setCodigo("java8");
		curso1.setDescripcion("Curso inicial de java 8");
		Curso curso2 = new Curso();
		curso2.setCodigo("angular");
		curso2.setDescripcion("Curso inicial de angular");
		Curso curso3 = new Curso();
		curso3.setCodigo("microservicios");
		curso3.setDescripcion("Curso con springboot y springcloud");
		cursos.add(curso1);
		cursos.add(curso2);
		cursos.add(curso3);
		response.setCursos(cursos);
		return Single.fromCallable(() -> response);
	}

}
