package com.everis.services.cross.service;

import com.everis.services.cross.api.response.RazonSocial;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.api.response.ResponseEstudiante;

import io.reactivex.Single;

public interface ApiCrossService {

  public Single<Response> obtenerRespuestaCorporateLoans(String corporateLoanId); 
  
  public Single<ResponseEstudiante> obtenerRespuestaDatosEstudiante(String dni); 
  //mod
  public Single<ResponseEstudiante> obtenerRespuestaTrabajo(String dni);
  
  public Single<RazonSocial> obtenerRespuestaRazonSocial(String codigo);
}
