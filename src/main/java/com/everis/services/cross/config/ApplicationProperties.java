package com.everis.services.cross.config;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
@ToString
public class ApplicationProperties {

  @NotNull
  @Value("${dni1}")
  private String dni1;
  
  @NotNull
  @Value("${dni2}")
  private String dni2;
  
  @NotNull
  @Value("${dni3}")
  private String dni3;
}
