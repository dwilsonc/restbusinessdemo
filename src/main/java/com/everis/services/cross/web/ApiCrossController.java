package com.everis.services.cross.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.everis.services.cross.api.request.DniParam;
import com.everis.services.cross.api.request.IdSearchPathParam;
import com.everis.services.cross.api.response.RazonSocial;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.api.response.ResponseEstudiante;
import com.everis.services.cross.service.ApiCrossService;

import io.reactivex.Single;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/api/cross/v1")
@Slf4j
public class ApiCrossController {

  @Autowired
  private ApiCrossService servicio;

  /**
   * sendToHostBean.
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value = "Consulta gen�rica de colocaciones por cliente (Cr�ditos Consist)."
          + " Permite realizar consultas por c�digo de la l�nea de cr�dito. Los "
          + "datos de salida incluyen los principales campos asociados al desembolso.",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET",
      notes = "classpath:swagger/notes/apicross.md")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecut� satisfactoriamente.",
          response = Response.class)})
  @GetMapping(value = "/obtenerDatos/{id}")
  public Single<Response> obtenerDatos(
      @PathVariable("id") IdSearchPathParam pathParams) {

    Single<Response> response = null;

    try {
      log.info("PathVariable: "+ pathParams.getCorporateLoanId());
      response =
          servicio.obtenerRespuestaCorporateLoans(pathParams.getCorporateLoanId());
//      log.info("Response: " + EverisUtil.toJson(response));
    } catch (Exception e) {
      log.error("Error: " + e);
    }

    return response;
  }
  
  /**
   * sendToHostBean.
   */
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value = "Consulta generica que obtiene los datos del estudiante",
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, response = Object.class, httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "Se ejecut� satisfactoriamente.",
          response = Response.class)})
  @GetMapping(value = "/obtenerDatosEstudiante/{dni}")
  public Single<ResponseEstudiante> obtenerDatosDelEstudiante(
      @PathVariable("dni") DniParam pathParams) {

    Single<ResponseEstudiante> response = null;

    try {
      log.info("PathVariable: "+ pathParams.getDni());
      response =
          servicio.obtenerRespuestaDatosEstudiante(pathParams.getDni());
//      log.info("Response: " + EverisUtil.toJson(response));
    } catch (Exception e) {
      log.error("Error: " + e);
    }

    return response;
  }
  
  //Mod
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value="Cosulta sobre trabajo",
      produces=MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, response= Object.class,httpMethod="GET")
  @ApiResponses({
    @ApiResponse(code=200,message="Se ejecuto exitosamente.",
      response = Response.class)})
@GetMapping(value="/obtener_datotrabajo/{dni}")
  public Single<ResponseEstudiante> obtener_datosTrabajo(
      @PathVariable("dni") DniParam pathParm){
    Single<ResponseEstudiante> response=null;
    try {
      log.info("PathVariable: "+ pathParm.getDni());
      response=
          servicio.obtenerRespuestaTrabajo(pathParm.getDni());
    } catch (Exception e) {
      log.error("Error: " + e);
    }
    return response;
  }
  
  //trabajo
  @ResponseStatus(HttpStatus.OK)
  @ApiOperation(
      value="Cosulta sobre direcciones",
      produces=MediaType.APPLICATION_JSON_UTF8_VALUE,
      consumes=MediaType.APPLICATION_JSON_UTF8_VALUE, response= Object.class,httpMethod="GET")
  @ApiResponses({
    @ApiResponse(code=200,message="Se ejecuto exitosamente.",
      response = Response.class)})
@GetMapping(value="/obtener_direccion/{dni}")
  public Single<RazonSocial> razonSocial(
      @PathVariable("dni") DniParam pathParm){
    Single<RazonSocial> response=null;
    try {
      log.info("PathVariable: "+ pathParm.getDni());
      response=
          servicio.obtenerRespuestaRazonSocial(pathParm.getDni());
    } catch (Exception e) {
      log.error("Error: " + e);
    }
    return response;
  }
      
  
}
