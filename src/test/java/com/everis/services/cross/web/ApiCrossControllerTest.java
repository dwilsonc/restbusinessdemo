package com.everis.services.cross.web;

import static org.mockito.Mockito.when;
import java.io.IOException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import com.everis.services.cross.api.request.DniParam;
import com.everis.services.cross.api.request.IdSearchPathParam;
import com.everis.services.cross.api.response.RazonSocial;
import com.everis.services.cross.api.response.Response;
import com.everis.services.cross.api.response.ResponseEstudiante;
import com.everis.services.cross.service.ApiCrossService;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

@RunWith(MockitoJUnitRunner.class)

public class ApiCrossControllerTest {
  @InjectMocks
  ApiCrossController controller;
  
  @Mock
  ApiCrossService service;
  @Test
  public  void simulateCrossTest() throws IOException {
    when(service.obtenerRespuestaDatosEstudiante(Mockito.any()))
    .thenReturn(Single.just(new ResponseEstudiante()));
    
  TestObserver<ResponseEstudiante> test=controller.obtenerDatosDelEstudiante(new DniParam()).test(); 
  test.awaitTerminalEvent();
  }
  //Mod
  @Test
  public void simulateCrossTrabajoTest() throws IOException{
    when(service.obtenerRespuestaTrabajo(Mockito.any()))
    .thenReturn(Single.just(new ResponseEstudiante()));
    
    TestObserver<ResponseEstudiante> test=controller.obtener_datosTrabajo(new DniParam()).test();
    test.awaitTerminalEvent();
  }
  @Test
  public void simulateCrossObtenerDatos() throws IOException{
    when(service.obtenerRespuestaCorporateLoans(Mockito.any()))
    .thenReturn(Single.just(new Response()));
    
    TestObserver<Response> test=controller.obtenerDatos(new IdSearchPathParam()).test();
    test.awaitTerminalEvent();
  }
  @Test
  public void simulateCrossRazonSocial() throws IOException{
    when(service.obtenerRespuestaRazonSocial(Mockito.any()))
    .thenReturn(Single.just(new RazonSocial()));
    
    TestObserver<RazonSocial> test=controller.razonSocial(new DniParam()).test();
    test.awaitTerminalEvent();
  }
}
